const express = require('express');
const hbs = require('hbs');
const path = require('path');
const mongoose = require('mongoose');
const logger = require('morgan');
const createError = require('http-errors');
const cookieParser = require('cookie-parser');
const wordsBN = require('./public/db/words.bn');
const dictOALD = require('./public/db/dict.oald');
const { searchWordsBN, searchDictOALD, wordDetailsDictOALD } = require('./public/js/helper');
const bookmark = require('./src/bookmark');
const app = express();

// load required env variables for localhost
if (!process.env.NODE_ENV) {
    const dotenv = require('dotenv').config();
    if (dotenv.error) console.error(result.error);
}
// database 
mongoose.Promise = global.Promise;
mongoose.connect(process.env.MONGODB_URI, {
    useNewUrlParser:true 
});
mongoose.connection.on('error', (err) => {
    console.error(`MongoDB connection error: ${err}`);
    process.exit(1);
});
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');
hbs.registerPartials(__dirname + '/views/partials');
app.use(express.static(path.join(__dirname, 'public')));

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());


app.get('/definition', function(req, res) {
    if (req.query.q) {
        res.redirect('/definition/' + req.query.q + '/?q=' + req.query.q); 
    } else res.redirect('/'); 
})

app.get('/definition/:word', function(req, res) {
    let searchWord = req.params.word.toLowerCase().trim();
    let words = searchWordsBN(searchWord, wordsBN);
    
    if (!req.query.q) { // add /?q=searchWord to search in google 
        res.redirect(req.url + '?q=' + searchWord); 
    } else if (words.length === 0) {
        res.render('zzz', { word: 'zzz' });
    } else if (words.length > 1) { // similar words collection
        res.render('words', { words, searchWord: req.params.word });
    } else {
        let word = words[0];
        res.render('word', { word, wordOALD: wordDetailsDictOALD(searchWord, wordsBN, dictOALD) });
    }

})

app.get('/bookmark', bookmark)
app.get('/phrasebook', function(req, res) {
    res.render('phrasebook')
})


// catch 404 and forward to error handler
app.use(function(req, res, next) {
    next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
});

module.exports = app;
