"use strict";
var fullmode = true // true -> oald | false -> oald.topic


$(document).ready(function () {
    if (typeof topicTreeOALD !== 'undefined') topicTreeOALD();

    if (window.location.hash) {
        $('#display-board').text('loading....');
        var hashWord = window.location.hash.substring(1).toLowerCase().trim();
        $("#search-value").val(hashWord);
        if (fullmode) wordDetailsDictOALD(hashWord, wordsBN, dictOALD);
        else wordDetailsDictTopicOALD(hashWord, wordsBN, dictTopicOALD);
    }

    $("#search-button").click(function (e) {
        e.preventDefault();
        var searchWord = $("#search-value").val().toLowerCase().trim();
        if (searchWord) {
            $('#display-board').text('loading....');
            window.location.hash = searchWord;
            if (fullmode) wordDetailsDictOALD(searchWord, wordsBN, dictOALD);
            else wordDetailsDictTopicOALD(hashWord, wordsBN, dictTopicOALD);
        }
    });

    $('#dict-mode').click(function() {
        if ($("#dict-mode").is(':checked')) fullmode = false;
        else fullmode = true;
    });

    $("#browse-topic").click(function (e) {
        e.preventDefault();
        $('#display-board').text('loading....');
        topicTreeOALD();
    });

    $(".browse-topwords-oald").click(function (e) {
        e.preventDefault();
        $('#display-board').text('loading....');
        var amount = $(this).attr('data-value');
        if (fullmode) wordsDetailsDictOALD(0, eval('top'+amount), amount);
        else wordsDetailsDictTopicOALD(0, eval('top'+amount), amount);
    });

    $("#display-board").on("click", ".topwords-oald-pagination", function (e) {
        e.preventDefault();
        $('#display-board').text('loading....');
        var pagination = $(this).attr('data-pagination');
        var amount = $(this).attr('data-topwords-amount');
        if (fullmode) wordsDetailsDictOALD(pagination, eval('top'+amount), amount);
        else wordsDetailsDictTopicOALD(pagination, eval('top'+amount), amount);
    });

    $("#display-board").on("click", ".topic-list", function (e) {
        e.preventDefault();
        var selectTopic = $(this).text().toLowerCase().trim();
        console.log(selectTopic);
        viewTopic(selectTopic);
    });

    $("#display-board").on("click", ".search-arr-value", function (e) {
        e.preventDefault();
        $('#display-board').text('loading....');
        var selectWord = $(this).text().toLowerCase().trim();
        window.location.hash = selectWord;
        if (fullmode) wordDetailsDictOALD(selectWord, wordsBN, dictOALD);
        else wordDetailsDictTopicOALD(selectWord, wordsBN, dictTopicOALD);
    });

    let speedNormal = true;
    $("#display-board").on("click", ".audio-play", function (e) {
        e.preventDefault();
        let selector = "#audio-" + $(this).attr('href');
        if (speedNormal) {
            speedNormal = false;
            $(selector)[0].playbackRate = 0.65;
        } else {
            speedNormal = true;
            $(selector)[0].playbackRate = 1.0;
        }
        $(selector)[0].play();
    });

    /* list collapse and toggle */
    $("#display-board").on("click", ".collapse-list > li", function (e) {
        //console.log(e.target.nodeName);
        if((e.target.nodeName === 'LI' || e.target.nodeName === 'P') && e.target.className.split(' ')[0] === 'collapse-list-word') {
            if ($(this).hasClass('highlighted')) $(this).removeClass('highlighted');
            else $(this).addClass('highlighted')

            if ($(this).children().hasClass('non-highlighted')) $(this).children().removeClass('non-highlighted');
            else $(this).children().addClass('non-highlighted')

            $(this).children().toggle();
        }
    })

    $("#display-board").on("click", ".collapse-list ul > li", function (e) {
        // console.log(e.target.nodeName);
        e.preventDefault();
        if ($(this).children('a') && $(this).children('a').attr('data-path')) {
            $('#display-board').text('loading....');
            var topicPath = $(this).children('a').attr('data-path');
            console.log(topicPath);
            if (fullmode) wordsDetailsDictOALD(topicPath, topicOALD);
            else wordsDetailsDictTopicOALD(topicPath, topicOALD);
        }
    })

    $('button.close').click(function (e) {
        e.preventDefault();
        $("#search-value").val('');
    });

});


console.log('dict running....')


// var text = [];
// var temp = [];
// document.querySelectorAll('#wordlistsContentPanel li>span:first-child').forEach(function(item) {
//     console.log(item.innerText);
//     if (!temp.includes(item.innerText)) temp.push(item.innerText);
//     if (temp.length === 100) {
//         text.push(temp);
//         temp = [];
//     }
// })
// text.push(temp);

// var vLink = document.createElement('a');
// vBlob = new Blob([JSON.stringify(text)], {type: "text/plain;charset=utf-8"});
// vName = 'bwsd_raw_corpus_links.txt';
// vUrl = window.URL.createObjectURL(vBlob);
// vLink.setAttribute('href', vUrl);
// vLink.setAttribute('download', vName);
// vLink.click();
