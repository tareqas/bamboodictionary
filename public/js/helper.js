var wordsPath, voicePath;

if (typeof process !== 'undefined' && process.env) { //server
    wordsPath = 'https://gitlab.com/tareqas/bamboodictionary/raw/master/public/words';
    voicePath = 'https://gitlab.com/tareqas/bamboodictionary/raw/master/public/voice';
} else { // direct
    wordsPath = './words';
    voicePath = './voice';
}

// search word from Bangla Academy Dictionary | return []
function searchWordsBN(word, wordsBN) {
    word = word.toLowerCase().trim();
    var l = word.length;
    var words = [];
    for (; l > 0; l--) {
        if (wordsBN.includes(word)) {
            words[0] = word;
        } else if (l === 3) {
            words = wordsBN.filter(
                ww => new RegExp(`^(${word}).+`, 'g').test(ww)
            );
        } else {
            word = word.slice(0, -1);
        }

    }
    return words;

}

// search word from dictOALD
function searchDictOALD(word, dictOALD) {
    word = word.toLowerCase().trim();
    if (dictOALD[word]) return word;
    else return '';

}

// search word from dictTopicOALD
function searchDictTopicOALD(word, dictTopicOALD) {
    word = word.toLowerCase().trim();
    var words = [];
    if (dictTopicOALD[word]) {
        words[0] = word;
    } else {
        for (var i = 1; ; i++) {
            var index = word + '_' + i;
            if (dictTopicOALD[index]) {
                words.push(index);
            } else {
                break;
            }
        }
    }
    return words;

}

// display details for single searched word
function wordDetailsDictOALD(searchWord, wordsBN, dictOALD) {
    var words = searchWordsBN(searchWord, wordsBN);
    var str = `<div class="word-details">`;

    if (words.length === 0) {
        str += `<img class="img-fluid" src="${wordsPath}/zzz">`;
    } else if (words.length > 1) {
        words.forEach(function (value) {
            str += `<li class="list-group-item search-arr-value"><a href="#">${value}</a></li>`;
        })
        str += `<ul class="list-group list-group-flush">${str}</ul>`;
    } else {
        var word = words[0];
        var wordOALD = searchDictOALD(word, dictOALD);
        wordOALD = (wordOALD) ? dictOALD[wordOALD] : wordOALD;
        var wrd = word.replace(/'/g, '').replace(/ /g, '-');

        str += `<img class="img-fluid" src="${wordsPath}/${word}">`;
        
        if(wordOALD) {
            str += `<p>[${wordOALD.pos}] <a class="audio-play" href="${wrd}">[[play]]</a> ${wordOALD.pron}</p>
                    <audio id="audio-${wrd}" src="${voicePath}/${wrd}.ogg"></audio>`;
            wordOALD.meanings.forEach(function (mean) {
                str += `<div class="topic-words-seperator">
                            <p><b>Meaning: </b>${mean.meaning}</p>
                            <ul class="topic-examples">`;
                            mean.examples.forEach(function(example) {
                                str += `<li>${example}</li>`;
                            })
                str += `</ul></div>`;
            })
        } 
        else {
            str += `<p><a class="audio-play" href="${wrd}">[[play]]</a></p>
                    <audio id="audio-${wrd}" src="${voicePath}/${wrd}.ogg"></audio>`;
        }
        str += `</div>`;

    }
    if (typeof $ !== 'undefined') $('#display-board').html(str);
    else return str;

}

// display details for single searched word
function wordDetailsDictTopicOALD(searchWord, wordsBN, dictTopicOALD) {
    var words = searchWordsBN(searchWord, wordsBN);
    var str = `<div class="word-details">`;

    if (words.length === 0) {
        str += `<img class="img-fluid" src="${wordsPath}/zzz">`;
    } else if (words.length > 1) {
        words.forEach(function (value) {
            str += `<li class="list-group-item search-arr-value"><a href="#">${value}</a></li>`;
        })
        str += `<ul class="list-group list-group-flush">${str}</ul>`;
    } else {
        var word = words[0];
        var wordsOALD = searchDictTopicOALD(word, dictTopicOALD);
        var wrd = word.replace(/'/g, '').replace(/ /g, '-');

        if(wordsOALD.length)
            str += `<p><b>Category: </b>${dictTopicOALD[wordsOALD[0]].topic} > ${dictTopicOALD[wordsOALD[0]].section} > ${dictTopicOALD[wordsOALD[0]].subsection}</p>`;
        str += `<img class="img-fluid" src="${wordsPath}/${word}">`;
        
        if(wordsOALD.length) {
            str += `<p><a class="audio-play" href="${wrd}">[[play]]</a> ${dictTopicOALD[wordsOALD[0]].pronunciation}</p>
                    <audio id="audio-${wrd}" src="${voicePath}/${wrd}.ogg"></audio>`;
            wordsOALD.forEach(function (word2) {
                str += `<div class="topic-words-seperator">
                            <p><b>Def: </b>[${dictTopicOALD[word2].pos}] ${dictTopicOALD[word2].definition}</p>
                            <ul class="topic-examples">`;
                            dictTopicOALD[word2].examples.forEach(function(el) {
                                str += `<li>${el}</li>`;
                            })
                str += `</ul></div>`;
            })
        } 
        else {
            str += `<p><a class="audio-play" href="${wrd}">[[play]]</a></p>
                    <audio id="audio-${wrd}" src="${voicePath}/${wrd}.ogg"></audio>`;
        }
        str += `</div>`;

    }
    if (typeof $ !== 'undefined') $('#display-board').html(str);
    else return str;

}

// display topic lists (Topic dictTopicOALD)
function topicTreeOALD() {
    var str = '<ul class="collapse-list">';
    Object.keys(topicOALD).forEach(function (el) {
        str += `<li class="collapse-list-word">${el}`;
        Object.keys(topicOALD[el]).forEach(function (sec) {
            str += `<ul>${sec}`;
            Object.keys(topicOALD[el][sec]).forEach(function (subSec) {
                str += `<li>
                            <a href="#" data-path="${el}/${sec}/${subSec}">${subSec}</a>
                        </li>`;
            })
            str += `</ul>`;
        })
        str += `</li>`;
    })
    str += `</ul>`;
    if (typeof $ !== 'undefined') {
        $('#display-board').html(str);
        $(".collapse-list > li > ul").hide();
    } else return str;

}


function wordsDetailsDictOALD(topicIndex, givenWords, amount) { // amount for OALD top3000, top5000, top2000
    var words, str = '';
    var st = (typeof topicIndex === 'string') ? topicIndex.split('/') : '';
    
    if (st[0] && st[1] && givenWords[st[0]] && givenWords[st[0]][st[1]]) {
        words = topicOALD[st[0]][st[1]][st[2]];
        str += '<h4>' + st[0] + ' > ' + st[1] + ' > ' + st[2] + '</h4>';
    }
    else {
        topicIndex = parseInt(topicIndex);
        words = givenWords[topicIndex];
        str += '<div class="pagination">';
        for (var i = 0; i < givenWords.length; i++) {
            var _i = ((n) => n > 9 ? "" + n: "0" + n)(i+1);
            if (i === topicIndex) {
                str += `<a href="#" class="topwords-oald-pagination pagination-selected" ${(amount) ? 'data-topwords-amount="' + amount + '"' : ''} data-pagination="${i}">${_i}</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;`; 
            } else {
                str += `<a href="#" class="topwords-oald-pagination" ${(amount) ? 'data-topwords-amount="' + amount + '"' : ''} data-pagination="${i}">${_i}</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;`; 
            }
        }
        str += '</div><hr>'
    }

    str += '<ul class="collapse-list topic-words">';

    words.forEach(function(word) {
        var wordBn = searchWordsBN(word, wordsBN);
        var wordOALD = searchDictOALD(word, dictOALD);
        wordOALD = (wordOALD) ? dictOALD[wordOALD] : wordOALD;
        var wrd = word.replace(/'/g, '').replace(/ /g, '-').toLowerCase();

        str += `<li class="collapse-list-word">${word}
                    <ul><hr>
                        <div class="word-details">`;

        if (wordBn.length === 1) {
            if (wordBn[0] === word) {
                str += `<img class="img-fluid" src="${wordsPath}/${wrd}">`;
            } else {
                str += `<p>showing <b>${wordBn}</b> instead of <b>${word}</b></p>
                        <img class="img-fluid" src="${wordsPath}/${wordBn}">`
            }
        }

        if(wordOALD) {
            str += `<p>[${wordOALD.pos}] <a class="audio-play" href="${wrd}">[[play]]</a> ${wordOALD.pron}</p>
                    <audio id="audio-${wrd}" src="${voicePath}/${wrd}.ogg"></audio>`;
            wordOALD.meanings.forEach(function (mean) {
                str += `<div class="topic-words-seperator">
                            <p><b>Meaning: </b>${mean.meaning}</p>
                            <ul class="topic-examples">`;
                            mean.examples.forEach(function(example) {
                                str += `<li>${example}</li>`;
                            })
                str += `</ul></div>`;
            })
        }
        else {
            str += `<p><a class="audio-play" href="${wrd}">[[play]]</a></p>
                    <audio id="audio-${wrd}" src="${voicePath}/${wrd}.ogg"></audio>`;
        }

        str += `<p class="collapse-list-word topic-words-collapsed">collapse</p>`;
        str += `</div></ul></li>`; // end of .word-details

    })
    str += '</ul>'; // end of .collapse-list topic-words 
    if (typeof $ !== 'undefined') {
        $('#display-board').html(str);
        $(".collapse-list > li > ul").hide();
    } else return str;

}


function wordsDetailsDictTopicOALD(topicIndex, givenWords, amount) {
    var words, str = '';
    var st = (typeof topicIndex === 'string') ? topicIndex.split('/') : '';
    
    if (st[0] && st[1] && givenWords[st[0]] && givenWords[st[0]][st[1]]) {
        words = topicOALD[st[0]][st[1]][st[2]];
        str += '<h4>' + st[0] + ' > ' + st[1] + ' > ' + st[2] + '</h4>';
    }
    else {
        topicIndex = parseInt(topicIndex);
        words = givenWords[topicIndex];
        str += '<div class="pagination">';
        for (var i = 0; i < givenWords.length; i++) {
            var _i = ((n) => n > 9 ? "" + n: "0" + n)(i+1);
            if (i === topicIndex) {
                str += `<a href="#" class="topwords-oald-pagination pagination-selected" ${(amount) ? 'data-topwords-amount="' + amount + '"' : ''} data-pagination="${i}">${_i}</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;`; 
            } else {
                str += `<a href="#" class="topwords-oald-pagination" ${(amount) ? 'data-topwords-amount="' + amount + '"' : ''} data-pagination="${i}">${_i}</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;`; 
            }
        }
        str += '</div><hr>'
    }

    str += '<ul class="collapse-list topic-words">';

    words.forEach(function(word) {
        var wordBn = searchWordsBN(word, wordsBN);
        var wordsOALD = searchDictTopicOALD(word, dictTopicOALD);
        var wrd = word.replace(/'/g, '').replace(/ /g, '-').toLowerCase();

        str += `<li class="collapse-list-word">${word}
                    <ul><hr>
                        <div class="word-details">`;

        if (wordBn.length === 1) {
            if (wordBn[0] === word) {
                str += `<img class="img-fluid" src="${wordsPath}/${wrd}">`;
            } else {
                str += `<p>showing <b>${wordBn}</b> instead of <b>${word}</b></p>
                        <img class="img-fluid" src="${wordsPath}/${wordBn}">`
            }
        }

        str += `<p><a class="audio-play" href="${wrd}">[[play]]</a> ${(dictTopicOALD[wordsOALD[0]]) ? dictTopicOALD[wordsOALD[0]].pronunciation : ''}</p>
                <audio id="audio-${wrd}" src="${voicePath}/${wrd}.ogg"></audio>`

        wordsOALD.forEach(function (word2) {
            var word3 = dictTopicOALD[word2];
            str += `<div class="topic-words-seperator">`;
            str += (word3.pos && word3.definition) ? `<p><b>Def: </b>[${word3.pos}] ${word3.definition}</p>` : '';
            if (word3.examples.length) {
                str += `<ul class="topic-examples">`;
                word3.examples.forEach(function (example) {
                    str += `<li>${example}</li>`
                })
                str += `</ul><br>`;
            }
            str += `</div>`; // end of .topic-words-seperator
        });

        str += `<p class="collapse-list-word topic-words-collapsed">collapse</p>`;
        str += `</div></ul></li>`; // end of .word-details

    })
    str += '</ul>'; // end of .collapse-list topic-words 
    if (typeof $ !== 'undefined') {
        $('#display-board').html(str);
        $(".collapse-list > li > ul").hide();
    } else return str;

}

if (typeof module !== 'undefined' && module.exports) 
    module.exports = {
        searchWordsBN,
        searchDictOALD,
        searchDictTopicOALD,
        wordDetailsDictOALD,
        wordDetailsDictTopicOALD,
        topicTreeOALD,
        wordsDetailsDictOALD,
        wordsDetailsDictTopicOALD 
    };