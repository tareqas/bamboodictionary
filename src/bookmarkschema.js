const mongoose = require('mongoose');

const BookmarkSchema = mongoose.Schema({
    topic: {
        type: String,
        required: true,
    },
    words: {
        type: String,
        required: true,
        text: true
    }

});

module.exports = mongoose.model('bookmark', BookmarkSchema);
