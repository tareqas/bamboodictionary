// read word from Bangla Academy Dictionary
function readerBn(word, dictionaryBn) {
    var l = word.length;
    var words = [];
    for (; l > 0; l--) {
        if (dictionaryBn.includes(word)) {
            words[0] = word;
        } else if (l === 3) {
            words = dictionaryBn.filter(
                ww => new RegExp(`^(${word}).+`, 'g').test(ww)
            );
        } else {
            word = word.slice(0, -1);
        }

    }
    return words;

}

// read word from OALD topic dictionary
function readerTopic(word, dictionaryOALD) {
    var words = [];
    if (dictionaryOALD[word]) {
        words[0] = word;
    } else {
        for (var i = 1; ; i++) {
            var index = word + '_' + i;
            if (dictionaryOALD[index]) {
                words.push(index);
            } else {
                break;
            }
        }
    }
    return words;

}

module.exports = {
    readerBn, readerTopic
}