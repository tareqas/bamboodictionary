const Bookmark = require('./bookmarkschema');

module.exports = function (req, res) {
    let { topic, newword, password } = req.query;
    let dictionary = [], bag = [];

    Bookmark.find({}, function(err, bookmarks) {
        if (err) res.send('error occurred to fetch bookmarks from database');

        bookmarks.forEach(function (bookmark) {
            dictionary[bookmark.topic] = bookmark.words.split(',').sort().join(',');
            bag.push({
                topic: bookmark.topic,
                words: bookmark.words.split(',').join(', ')
            })
        })
        let topics = Object.keys(dictionary)

        // add new word to a topic
        if (topic && newword && process.env.PASSWORD === password) {
            if (dictionary[topic].split(',').includes(newword)) {
                res.render('bookmark/index', {
                    message: `"${newword}" has been already bookmarked`,
                    topics, bag, password, newword
                })

            }
            else {
                let updatedWords = dictionary[topic] + ',' + newword;
                Bookmark.update({ topic: topic}, { words: updatedWords }, function(err, keyvalue) {
                    if (err) {
                        res.send('[db_err] error occurred during saving');
                    }
                    else {
                        res.render('bookmark/index', {
                            message: `"${newword}" has been bookmarked`,
                            topics, bag, password, newword              
                        })
                    }
                });

            }

        }
        else {
            res.render('bookmark/index', {
                topics, bag, password, newword
            })
        }

    });
    
}

if (false) {
    let updatedWords = words;
    Bookmark.update({ topic: topic}, { words: updatedWords }, function(err, keyvalue) {
        if (err) res.send('[db_err] error occurred during saving');
    });

    Bookmark.create({
        topic: topic,
        words: "riot, skirmish"
    }, function(err, keyvalue) {
        if (err) res.send('[db_err] error occurred during saving');
    });

}